from flask import Flask
from flask import request
from flask import jsonify
from youtube_dl import YoutubeDL

app = Flask(__name__)

@app.route("/basic", methods=['POST'])
def get_basic_info():
    req_data = request.get_json()
    yt = YoutubeDL()
    yt.simulate = True
    info = yt.extract_info(url=req_data["url"],download=False)
    print(info.keys())
    return jsonify({"title":info["title"],"description":info["description"]})

@app.route("/advanced", methods=['POST'])
def get_advanced_info():
    req_data = request.get_json()
    yt = YoutubeDL()
    yt.simulate = True
    info = yt.extract_info(url=req_data["url"],download=False)
    formats =[]
    for f in info.get("formats"):
        print(f)
        formats.append({"format_id":f.get("format_id"),"ext":f.get("ext"),"format":f.get("format")})

    return jsonify({"title":info["title"],"description":info["description"],"fomats":formats})


app.run(host="0.0.0.0",port=8080)